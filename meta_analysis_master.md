---
output:
  html_document:
    keep_md: yes
    toc: yes
    toc_float: yes
    code_folding: hide  

---


### Systematic review and meta-analysis of the calibration of confidence judgments in individuals with schizophrenia spectrum disorders

#### Martin Rouy, Pauline Saliou, Ladislas Nalborczyk, Michael Pereira, Paul Roux, Nathan Faivre  

#### [doi: xxx](https://doi.org)  

#### [git repository](https://gitlab.com/nfaivre/meta_analysis_scz_public)  

#### [osf repository](https://osf.io/cfm5d)  

#### [pre-registration](https://www.crd.york.ac.uk/prospero/display_record.php?RecordID=188614)

## Background

Metacognitive deficits are well documented in schizophrenia spectrum disorders as a decreased capacity to adjust confidence to first-order performance in a cognitive task. Because metacognitive ability directly depends on first-order performance, observed metacognitive deficits might be driven by lower first-order performance. We aimed to determine the extent to which individuals with schizophrenia experience specific deficits when producing confidence judgments and examined whether studies controlling for first-order performance found metacognitive deficits of smaller magnitude.

Electronic databases were searched for studies published until April 24th 2020. We conducted a Bayesian meta-analysis of 43 studies comparing the calibration of confidence in 1458 individuals with schizophrenia compared to 1337 matched controls. Group analyses and meta-regressions quantified how metacognitive deficits depended on task performance, cognitive domains, clinical severity, and antipsychotic dosage.


## Main meta-analytic model
We started by fitting a Bayesian model with the study variable as random effect, and a mildly informative prior centered on -0.3.

```r
source(here("subscripts","fit_t2_all.R"))
summary(bmod1_tmp)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esMeta | se(sqrt(varMeta)) ~ 0 + Intercept + (1 | study) 
##    Data: wa (Number of observations: 44) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 44) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.54      0.09     0.39     0.73 1.00    19067    32212
## 
## Population-Level Effects: 
##           Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept    -0.63      0.09    -0.82    -0.45 1.00    18124    27436
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```


## Leave-One-Out Analysis
To ensure that this analysis was not biased by outliers, we ran the same model in a Leave-One-Out Analysis framework.

```r
source(here("subscripts","LOOA.R"))
print(looplot)
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

The LOO analysis indicated that the study from Kother et al. (2018) was strongly deviant since the amplitude of the metacognitive deficit computed in the fold which leaves out the Kother et al. study is decreased by almost 5 standard deviations. We exluded this study for all following analyses.

## Trimmed meta-analytic model
Therefore, we fitted the model again and a null model with a 0 intercept

```r
source(here("subscripts","fit_t2_trim.R"))
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

```r
# fit null model with 0 intercept
source(here("subscripts","fit_t2_null.R"))
summary(Rmod2)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esMeta | se(sqrt(varMeta)) ~ 0 + Intercept + (1 | study) 
##    Data: wa (Number of observations: 43) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 43) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.36      0.07     0.24     0.50 1.00    24797    37508
## 
## Population-Level Effects: 
##           Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept    -0.57      0.07    -0.71    -0.43 1.00    32980    41475
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```
This analysis reveals extreme evidence for a deficit of metacognition in scz

## Robustess analysis
Next, to assess whether this result was robust to variation in priors, we conducted a robustness analysis with varying values of prior dispersion (0.1, 1, 3, 10) for a fixed mean value = -0.3.

```r
source(here("subscripts","robustnessAnalysis.R"))
print(rp)
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

## Sub-group Analyses
Of note all subgroup analyses were motivated by high levels of heterogeneity (not shown here)

```r
source(here("subscripts","heterogeneity.R"))
```
### Performance-Matching
Having shown the existence of a global metacognitive deficit in schizophrenia, our goal was to test if experiments that controlled for first-order performance found smaller metacognitive deficits than those that did not.

First we determined the magnitude of the cognitive deficit in scz in order to justify our suspicion of contamination of the metacognitive accuracy by first-order performance.

```r
# cognitive deficit
source(here("subscripts","fit_t1_trim.R"))
summary(bmodt1)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esT1 | se(sqrt(varT1)) ~ 0 + Intercept + (1 | study) 
##    Data: wa[!is.na(wa$esT1) & wa$study != "Kim 2010", ] (Number of observations: 39) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 39) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.27      0.07     0.12     0.42 1.00    19932    20622
## 
## Population-Level Effects: 
##           Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept    -0.64      0.06    -0.77    -0.51 1.00    33422    39907
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```

```r
# cognitive deficit ~ cognitive domain
# source(here("subscripts","fit_t1_domains.R"))
# summary(bdomt1)
```
The first-order cognitive deficit is highly supported by the summary effect size, given the prior and the data.

Our main hypothesis was also supported, as studies controlling for first-order performance report metacognitive deficits that are about twice smaller than those that did not.

```r
source(here("subscripts","fit_t2_match.R"))
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

Among studies that did control for first-order performance, evidence in favor of a metacognitive deficit was not conclusive. We also quantified the relationship between cognitive and metacognitive deficits among studies which did not control for first-order performance. This analysis showed a positive correlation, that was robust to the choice of priors.


```r
source(here("subscripts","fit_t2_matchedonly.R"))
```


```r
source(here("subscripts","fit_t2_t1.R"))
print(patchwork_fig2)
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

```r
### Robustness analysis
source(here("subscripts","robustnessAnalysis_metaregt1.R"))
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-9-2.png)<!-- -->

### Cognitive domains
As per our preregistered plan, we assessed possible differences in metacognitive deficits across cognitive domains (perception, memory, others). Results revealed stronger metacognitive deficits in the memory domain, although most of these studies did not control for first-order performance.

```r
source(here("subscripts","fit_t2_domains.R"))
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

### Clinical Scores
We also examined the relationship between the metacognitive deficit and several clinical variables including the PANSS (total, positive and negative scores, and the antipsychotic dosage)

```r
source(here("subscripts","fit_t2_clinicalScores.R"))
print('Positive scores')
```

```
## [1] "Positive scores"
```

```r
summary(bps)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esMeta | se(sqrt(varMeta)) ~ 0 + Intercept + PS_Norm + (1 | study) 
##    Data: waps (Number of observations: 32) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 32) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.35      0.08     0.19     0.53 1.00    22403    27532
## 
## Population-Level Effects: 
##           Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept    -0.57      0.08    -0.74    -0.41 1.00    23513    31765
## PS_Norm       0.07      0.07    -0.07     0.20 1.00    32075    40592
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```

```r
hypothesis(bps, hypothesis = "PS_Norm = 0",seed = 123)
```

```
## Hypothesis Tests for class b:
##      Hypothesis Estimate Est.Error CI.Lower CI.Upper Evid.Ratio Post.Prob Star
## 1 (PS_Norm) = 0     0.07      0.07    -0.07      0.2       1.14      0.53     
## ---
## 'CI': 90%-CI for one-sided and 95%-CI for two-sided hypotheses.
## '*': For one-sided hypotheses, the posterior probability exceeds 95%;
## for two-sided hypotheses, the value tested against lies outside the 95%-CI.
## Posterior probabilities of point hypotheses assume equal prior probabilities.
```

```r
print('Negative scores')
```

```
## [1] "Negative scores"
```

```r
summary(bns)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esMeta | se(sqrt(varMeta)) ~ 0 + Intercept + NS_Norm + (1 | study) 
##    Data: wans (Number of observations: 33) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 33) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.34      0.08     0.19     0.52 1.00    23261    29194
## 
## Population-Level Effects: 
##           Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept    -0.57      0.08    -0.73    -0.41 1.00    32946    40351
## NS_Norm       0.03      0.06    -0.09     0.16 1.00    51243    46623
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```

```r
hypothesis(bns, hypothesis = "NS_Norm = 0",seed = 123)
```

```
## Hypothesis Tests for class b:
##      Hypothesis Estimate Est.Error CI.Lower CI.Upper Evid.Ratio Post.Prob Star
## 1 (NS_Norm) = 0     0.03      0.06    -0.09     0.16       1.32      0.57     
## ---
## 'CI': 90%-CI for one-sided and 95%-CI for two-sided hypotheses.
## '*': For one-sided hypotheses, the posterior probability exceeds 95%;
## for two-sided hypotheses, the value tested against lies outside the 95%-CI.
## Posterior probabilities of point hypotheses assume equal prior probabilities.
```

```r
print('Antipsychotic dosage')
```

```
## [1] "Antipsychotic dosage"
```

```r
summary(btreat)
```

```
##  Family: gaussian 
##   Links: mu = identity; sigma = identity 
## Formula: esMeta | se(sqrt(varMeta)) ~ 0 + Intercept + treat_Norm + (1 | study) 
##    Data: wtreat (Number of observations: 20) 
## Samples: 4 chains, each with iter = 20000; warmup = 5000; thin = 1;
##          total post-warmup samples = 60000
## 
## Group-Level Effects: 
## ~study (Number of levels: 20) 
##               Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## sd(Intercept)     0.37      0.12     0.16     0.62 1.00    19282    19022
## 
## Population-Level Effects: 
##            Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
## Intercept     -0.60      0.11    -0.81    -0.38 1.00    26849    33428
## treat_Norm    -0.00      0.01    -0.01     0.01 1.00    88143    45602
## 
## Samples were drawn using sampling(NUTS). For each parameter, Bulk_ESS
## and Tail_ESS are effective sample size measures, and Rhat is the potential
## scale reduction factor on split chains (at convergence, Rhat = 1).
```

```r
hypothesis(btreat, hypothesis = "treat_Norm = 0",seed = 123)
```

```
## Hypothesis Tests for class b:
##         Hypothesis Estimate Est.Error CI.Lower CI.Upper Evid.Ratio Post.Prob
## 1 (treat_Norm) = 0        0      0.01    -0.01     0.01       0.98      0.49
##   Star
## 1     
## ---
## 'CI': 90%-CI for one-sided and 95%-CI for two-sided hypotheses.
## '*': For one-sided hypotheses, the posterior probability exceeds 95%;
## for two-sided hypotheses, the value tested against lies outside the 95%-CI.
## Posterior probabilities of point hypotheses assume equal prior probabilities.
```

```r
print(patchwork_t2clinical)
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

None of these models revealed conclusive evidence for a correlation.

## Bias analaysis

Finally, we assessed the risk of bias among our study sample using different tools:

#### Newcastle Ottawa scale
This is a tool used for assessing the quality of studies along several dimensions. The quality varied in our sample, with a non negligible number of studys rated as poor.

```r
# Newcastle-Ottawa Scale contains 8 items within 3 domain and the total maximum score is 9.
# A study with score from
# 7-9, has high quality,
# 4-6, high risk,
# and 0-3 very high risk of bias. #Research gate. Ghadah Alrumaykhan

source(here('subscripts','NOS.R'))

print(NOSplot)
```

![](meta_analysis_master_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

```r
#ICC total
sprintf("ICC global agreement score reached by PS & MR: %.04s",iccTotal)
```

```
## [1] "ICC global agreement score reached by PS & MR: 0.55"
```

```r
#Interpretation : 0.5 < ICC score < 0.75 = average agreement

#ICC by question
# scores_good
# scores_weak
```

#### Funnel plot and P-curve
The absence of assymetry on this plot suggests the absence of strong publication bias.
The shape of the P-curve suggests an absence of p-hacking.


```
## 
## Regression Test for Funnel Plot Asymmetry
## 
## model:     mixed-effects meta-regression model
## predictor: standard error
## 
## test for funnel plot asymmetry: z = -0.0814, p = 0.9351
```

   
![](meta_analysis_master_files/figure-html/unnamed-chunk-13-1.png)<!-- -->
## Conclusions
We provide evidence for the existence of a deficit in the calibration of confidence judgments in schizophrenia, which is inflated due to non-equated first-order performance. Thus, efforts should be made to develop experimental protocols accounting for lower first-order performance in schizophrenia.

## Session Info

```r
sessionInfo()
```

```
## R version 4.0.3 (2020-10-10)
## Platform: x86_64-pc-linux-gnu (64-bit)
## Running under: Ubuntu 20.10
## 
## Matrix products: default
## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
## LAPACK: /usr/lib/x86_64-linux-gnu/openblas-openmp/liblapack.so.3
## 
## locale:
##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
##  [3] LC_TIME=fr_FR.UTF-8        LC_COLLATE=en_US.UTF-8    
##  [5] LC_MONETARY=fr_FR.UTF-8    LC_MESSAGES=en_US.UTF-8   
##  [7] LC_PAPER=fr_FR.UTF-8       LC_NAME=C                 
##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
## [11] LC_MEASUREMENT=fr_FR.UTF-8 LC_IDENTIFICATION=C       
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
##  [1] bayesboot_0.2.2      patchwork_1.0.0.9000 poibin_1.5          
##  [4] BEST_0.5.2           HDInterval_0.2.2     ggridges_0.5.2      
##  [7] tidybayes_2.1.1      metafor_2.4-0        Matrix_1.2-18       
## [10] meta_4.15-1          broom_0.5.6          RColorBrewer_1.1-2  
## [13] officer_0.3.9        flextable_0.5.9      ICCbin_1.1.1        
## [16] irr_0.84.1           lpSolve_5.6.15       esc_0.5.1           
## [19] brms_2.12.0          Rcpp_1.0.4.6         forcats_0.5.0       
## [22] stringr_1.4.0        dplyr_1.0.0          purrr_0.3.4         
## [25] tidyr_1.0.3          tibble_3.0.1         ggplot2_3.3.0       
## [28] tidyverse_1.3.0.9000 readr_1.3.1          here_0.1            
## 
## loaded via a namespace (and not attached):
##   [1] readxl_1.3.1         uuid_0.1-4           backports_1.1.6     
##   [4] Hmisc_4.4-0          systemfonts_0.2.1    plyr_1.8.6          
##   [7] igraph_1.2.5         splines_4.0.3        svUnit_1.0.3        
##  [10] crosstalk_1.1.0.1    rstantools_2.0.0     inline_0.3.15       
##  [13] digest_0.6.25        htmltools_0.4.0      rsconnect_0.8.16    
##  [16] fansi_0.4.1          checkmate_2.0.0      magrittr_1.5        
##  [19] cluster_2.1.0        modelr_0.1.7         matrixStats_0.56.0  
##  [22] xts_0.12-0           prettyunits_1.1.1    jpeg_0.1-8.1        
##  [25] colorspace_1.4-1     rvest_0.3.5          ggdist_2.1.1        
##  [28] haven_2.2.0          xfun_0.13            callr_3.4.3         
##  [31] crayon_1.3.4         jsonlite_1.6.1       lme4_1.1-23.9000    
##  [34] survival_3.2-7       zoo_1.8-8            glue_1.4.0          
##  [37] gtable_0.3.0         pkgbuild_1.0.8       rstan_2.19.3        
##  [40] abind_1.4-5          scales_1.1.0         mvtnorm_1.1-0       
##  [43] DBI_1.1.0            miniUI_0.1.1.1       htmlTable_1.13.3    
##  [46] xtable_1.8-4         foreign_0.8-79       Formula_1.2-3       
##  [49] stats4_4.0.3         StanHeaders_2.19.2   DT_0.13             
##  [52] htmlwidgets_1.5.1    httr_1.4.1           threejs_0.3.3       
##  [55] arrayhelpers_1.1-0   acepack_1.4.1        ellipsis_0.3.0      
##  [58] farver_2.0.3         pkgconfig_2.0.3      loo_2.2.0           
##  [61] nnet_7.3-14          dbplyr_1.4.3         labeling_0.3        
##  [64] tidyselect_1.1.0     rlang_0.4.7          reshape2_1.4.4      
##  [67] later_1.0.0          munsell_0.5.0        cellranger_1.1.0    
##  [70] tools_4.0.3          cli_2.0.2            generics_0.0.2      
##  [73] evaluate_0.14        fastmap_1.0.1        yaml_2.2.1          
##  [76] processx_3.4.2       knitr_1.28           fs_1.4.1            
##  [79] zip_2.0.4            nlme_3.1-149         mime_0.9            
##  [82] xml2_1.3.2           compiler_4.0.3       bayesplot_1.7.2     
##  [85] shinythemes_1.1.2    rstudioapi_0.11      png_0.1-7           
##  [88] reprex_0.3.0         statmod_1.4.34       stringi_1.4.6       
##  [91] ps_1.3.3             Brobdingnag_1.2-6    gdtools_0.2.2       
##  [94] lattice_0.20-41      nloptr_1.2.2.1       markdown_1.1        
##  [97] shinyjs_1.1          vctrs_0.3.1          CompQuadForm_1.4.3  
## [100] pillar_1.4.4         lifecycle_0.2.0      bridgesampling_1.0-0
## [103] data.table_1.12.8    httpuv_1.5.2         latticeExtra_0.6-29 
## [106] R6_2.4.1             promises_1.1.0       gridExtra_2.3       
## [109] rjags_4-10           boot_1.3-25          colourpicker_1.0    
## [112] MASS_7.3-53          gtools_3.8.2         assertthat_0.2.1    
## [115] rprojroot_1.3-2      withr_2.2.0          shinystan_2.5.0     
## [118] mgcv_1.8-33          parallel_4.0.3       hms_0.5.3           
## [121] rpart_4.1-15         grid_4.0.3           coda_0.19-3         
## [124] minqa_1.2.4          rmarkdown_2.1        shiny_1.4.0.2       
## [127] lubridate_1.7.8      base64enc_0.1-3      dygraphs_1.1.1.6
```
