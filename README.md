# meta_analysis_scz_public


**Systematic review and meta-analysis of metacognitive abilities in individuals with schizophrenia spectrum disorders**

Martin Rouy, Pauline Saliou, Ladislas Nalborczyk, Michael Pereira, Paul Roux, Nathan Faivre  

+ [doi: 10.1016/j.neubiorev.2021.03.017](https://doi.org/10.1016/j.neubiorev.2021.03.017)  

+ [git repository](https://gitlab.com/nfaivre/meta_analysis_scz_public)  

+ [osf repository](https://osf.io/cfm5d)  

+ [pre-registration](https://www.crd.york.ac.uk/prospero/display_record.php?RecordID=188614)


## This repository contains:  

+ data: bibliographic and bias assessment data

+ functions: R utilitary functions 

+ models: copy of brms models (not on git)

+ plots: main plots in pdf

+ subscripts: colletion of R scripts called by meta_analysis_master.md



