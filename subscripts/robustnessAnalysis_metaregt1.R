# intercept only model
prior00 <- c(
  set_prior("normal(-0.3, 1)", coef = "Intercept"),
  set_prior('cauchy(0, 0.1)', class = 'sd')  
)
if (redo_models==1){
  file.remove(here("models","Rmod00.rds"))
}
Rmod00 <- brm(
  esMeta | se(sqrt(varMeta) ) ~ 0 + Intercept  + (1|study),
  data = wa[!is.na(wa$esT1) & wa$study != "Kim2010" & wa$control_type1==FALSE,],
  prior = prior00,
  sample_prior = TRUE, 
  save_all_pars = TRUE,
  chains = 4,
  warmup = 5000,
  iter = 20000,
  cores = parallel::detectCores(),
  control = list(adapt_delta = .99),
  file=here("models","Rmod00"))
summary(Rmod00) 

if (redo_robust ==1){
  # Varying prior parameters
  priors_m = c(0, 0.3, 1)*ratio # ratio of range(t1Norm)/range(esT1)
  priors_sd = c(0.1,1,10)*ratio
  
  for (i in 1:3){
    for (j in 1:3){
      m = priors_m[i]
      sd = priors_sd[j]
      
      priorS <- c(
        set_prior("normal(-0.3, 1)", coef = "Intercept"),
        set_prior(paste0("normal(", m, ", ", sd, ")"), class = "b"),
        set_prior('cauchy(0, 0.1)', class = 'sd')  
      )
      
      assign(paste("Rmod",i,j,sep=""), brm(
        esMeta | se(sqrt(varMeta) ) ~ 0 + Intercept + t1Norm + (1|study),
        data = wat1,
        prior = priorS,
        sample_prior = TRUE,
        save_all_pars = TRUE,
        chains = 4,
        warmup = 5000,
        iter = 20000,
        cores = parallel::detectCores(),
        control = list(adapt_delta = .99),
        file=here("models",paste("Rmod",i,j,sep="")))
      )
    }
  }
  
  bfij = list()
  counter=1
  for (i in 1:3){
    for (j in 1:3){
      model <- readRDS(here("models",paste("Rmod",i,j,".rds",sep="")))
      bf <- bayes_factor(
        Rmod00, model,
        repetitions = 1e2, cores = parallel::detectCores())
      bfij[[counter]] = data.frame(bf$bf_median_based)
      counter = counter+1
    }
  }
  BFij = bind_rows(bfij)
  names(BFij) <- "BF01"
  
  m = list()
  sd = list()
  robust = list()
  counter=1
  for (i in 1:3){
    for (j in 1:3){
      model <- readRDS(here("models",paste("Rmod",i,j,".rds",sep="")))
      tmp = summary(model)
      robust[[counter]] <- tmp$fixed[2,c(1,3,4)]
      m[[counter]] <- data.frame(priors_m[i])
      sd[[counter]] <- data.frame(priors_sd[j])
      counter = counter +1
    }
  }
  robust = bind_rows(robust)
  m = bind_rows(m)
  sd = bind_rows(sd)
  names(m)<- "m"
  names(sd)<- "sd"
  robust$m = m$m
  robust$sd = sd$sd
  robust$BF01 <- BFij$BF01
  
  save(robust,file=here("data","robustAnalysis_t1.rda"))
} else {
  load(here("data","robustAnalysis_t1.rda"))
}

names(robust) <- c("Estimate","lower","upper","mean","sd","BF01")
rob <- robust
robust$sd <- as.factor(round(robust$sd,2))
rob_plot = ggplot(data = robust, aes(x = mean,y = Estimate, col=sd)) + 
  geom_point(aes(group=sd),position=position_dodge(0.1)) + 
  geom_line(position=position_dodge(0.1)) +
  geom_errorbar(aes(ymin = lower,ymax = upper),width=0.1,position=position_dodge(0.1)) +
  geom_vline(xintercept = 0.56, col="red",linetype="dashed")+
  labs(x="Prior mean", y=bquote(beta),col="Prior SD")+
  scale_x_continuous(breaks=c(0.56,round(priors_m,2)))+
  coord_cartesian(ylim = c(-0.1,0.6))
print(rob_plot)

rob <- rob[,c("sd","mean","Estimate","lower","upper","BF01")]
rob <- as.data.frame(rob)
rob <- round(rob, 3)
rob$BF01 <- formatC(rob$BF01, format="g")

robust2 = rob %>% arrange(sd) %>%  flextable() %>%
  set_header_labels(rob,sd='Prior',mean='Prior',Estimate = 'Estimate',lower='95%CrI',upper='95%CrI', BF01 = "BF01") %>% 
  add_header(top=F,sd='SD',mean='Mean',Estimate = 'Estimate',lower='Lower',upper='Upper', BF01 = "BF01") %>% 
  merge_v(part = "header") %>% merge_at( i = 1, j = 1:2, part = "header") %>% merge_at( i = 1, j = 4:5, part = "header")%>%  bold( part = "header") %>% 
  align(align = "center", part = "all" ) %>% autofit() %>% 
  fontsize(.,i = NULL, j = NULL, size = 8, part = "body") %>%
  fontsize(.,i = NULL, j = NULL, size = 8, part = "header")

doc <- read_docx()
doc <- body_add_flextable(doc, value = robust2)
#print(doc, target = here("plots","table_robust2.docx"))
