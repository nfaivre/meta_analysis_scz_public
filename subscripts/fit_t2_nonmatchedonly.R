prior1bis <- c(
  prior(normal(-0.3, 1), coef = "Intercept"),
  prior(cauchy(0, 0.1), class = sd)
)

if (redo_models==1){
  file.remove(here('models','bmatcht1F.rds'))
}
bmatcht1F <- brm(
  esMeta | se(sqrt(varMeta) ) ~ 0 + Intercept   + (1|study),
  data = wa[wa$control_type1 == FALSE,],
  prior = prior1bis,
  sample_prior = TRUE,
  save_all_pars = TRUE,
  chains = 4,
  warmup = 5000,
  iter = 20000,
  cores = parallel::detectCores(),
  control = list(adapt_delta = .99),
  file=here("models","bmatcht1F")
)

matchF_est = posterior_summary(bmatcht1F, pars = c("^b_", "^sd_"), probs = c(0.025,0.975))
post_matchF = posterior_samples(bmatcht1F,pars="^b_")

df_postF= data.frame(estimates = post_matchF[,1], distribution ="posterior")
df_priorF= data.frame(estimates = rnorm(n=60000,mean = -0.3, sd = 1),distribution ="prior")
df_ppF = rbind(df_postF,df_priorF)

mp = round(matchF_est[1],2)
plot_matchF = ggplot(df_ppF,aes(x=estimates,fill=distribution))+
  geom_density(alpha=0.6)+
  theme(legend.position = "bottom")+
  geom_vline(xintercept = 0, color = "black") + 
  geom_vline(xintercept = mp, color = "black",linetype="dashed") + 
  geom_segment(show.legend = FALSE, 
               aes(x=matchF_est[1,3],
                   y=-0.02,
                   xend=matchF_est[1,4],
                   yend = -0.02), size=2, color="black")+
  coord_cartesian(xlim=c(-0.8,0.2))+
  scale_fill_manual(values=c('grey','NA'))+
  labs(x=xLab, y=yLab)+
  ggtitle(label="A")+
  scale_x_continuous(breaks=c(0,mp,1))

print(plot_matchF)
ggsave(here('plots','matchF.pdf'),device = "pdf",width=4, height=3)

plotPost(post_matchF[,1],
         credMass=0.95,
         compVal=0,
         xlab = "Metacognitive deficit (Hedge's g)",
         col = "#b3cde0")

# Bayes Factor
hyp_matchF = hypothesis(bmatcht1F,"Intercept = 0",class = "b")

sprintf("Evidence supporting the hypothesis that the metacognitive deficit is null when first-order performance is controlled is inconclusive, BF01 = %s",formatC(hyp_matchF$hypothesis$Evid.Ratio, format = "e", digits = 2))  
BF10 = formatC(1/(hyp_matchF$hypothesis$Evid.Ratio),format="e",digit=2)

# Forest plot

if (do_forest){
  forest_match = my_forest(bmatcht1F)
  print(forest_match)
}
